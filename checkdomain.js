var Q = require('q');
var querysql = require('./mysql.js');
var child_process = require('child_process');
var events = require('events');
var myEmitter = new events.EventEmitter();
var fs = require('fs');

var checkempty = function(domain, git){
        return Q.promise((res) => {
                if(domain == undefined || git == undefined || domain == '' || git == ''){
                        res('error');
                }else{
                        res('oke');
                }
        });
}

var checkdomain = function(domain, git){
	return Q.promise((res) => {
		checkempty(domain, git).then(function(data){
			if(data == 'error'){
				myEmitter.emit('returndata', ({'error': 'domainfail', 'userlogin': '', 'password': '', 'domain': 'domain'}));
				return;
			}else{
				return  data;
			}
			
		}).then(function(data){
			if(data == 'oke'){
				fs.exists('domain/'+ domain, (exists) => {
					if(exists){
						myEmitter.emit('returndata', ({'error': 'domainexists', 'userlogin': '', 'password': '', 'domain': domain}));
					}
					else{
						child_process.exec('git clone' + " " + git + " " + 'domain/' + domain, (error, stdout, stderr) => {
							if(error){
								return res('notclonegit');
							}
							else{
								child_process.exec('rm -rf domain/' + domain + '/.git');
								return res('createdatabase');
							}
						});
					}
				});	
			}	
		});
	});
}



var exportenv = function(domain, user, password){
	if(user == undefined || password == undefined){
		console.log('undefined');
	}else{
	let path = 'domain/' + domain + '/env.php';
	let contentfile = '<?php\n' + '\$_ENV = [\n' + '   "WP_HOME" => "http://' + domain + '",\n' + '   "WP_SITEURL" => "http://' + domain + '",\n' + '   "DB_NAME" => "' + user + '",\n' + '   "DB_USER" => "' + user + '",\n' + '   "DB_PASSWORD" => "' + password + '",\n' + '   "DB_HOST" => "localhost",\n' + '];\n' + '?>';
	fs.writeFile(path, contentfile, (err, data) => {
		if(err){
			myEmitter.emit('returndata', ({'error': 'notcreatefileenv', 'userlogin': '', 'password': '', 'domain': domain}));
		}
	});
	}
}

var runweb = function(domain, git, callback){
	checkdomain(domain, git).then(function(statusdomain){
	        if(statusdomain = 'createdatabase'){
			querysql.runmysql(domain, (data) => {
				if(data.status == 'successfully'){
					exportenv(domain, data.user, data.password);
					let queryimport = "mysql -u " + data.database + " -p" + data.password + " -h localhost " + data.user + " < " + "domain/" + domain + "/database/database.sql" 
					child_process.exec(queryimport, (err, out) => {
						if(err){
							myEmitter.emit('returndata', ({'error': 'errorimportdb', 'userlogin': '', 'password': '', 'domain': domain}));
						}else{
							querysql.creatuser(data.user, data.password, data.domain, (user) => {
								myEmitter.emit('returndata', user);	
							});
						}
					});
				}else{
					myEmitter.emit('returndata', ({'error': data.status, 'userlogin': '', 'password': '', 'domain': domain}));
				}
			});
		}else{
			myEmitter.emit('returndata', ({'error': statusdomain, 'userlogin': '', 'password': '', 'domain': domain}));
		}
	});
	myEmitter.on('returndata', (data) => {
		if(data.error == ''){		
			return callback(data);
		}else if(data.error == 'domainexists' || data.error == 'domainfail'){
			return callback(data);
		}else{
			deletedomain(data.domain);
			return callback(data);
		}
	});
}

var deletedomain = function(domain){
	let path = 'domain/'+ domain;
	child_process.exec('rm -rf '+ path);
	querysql.deletemysql(domain);
}

exports.runweb = runweb;
exports.checkdomain = checkdomain;
